#include <iostream>
using namespace std;

// single inherit

// class shape
// {
//     protected:
//         int width;
//         int height;
//     public:
//         void set_values(int w,int h)
//         {
//             width = w;
//             height = h;
//         }
// };
// class rectangle : public shape
// {
//     public:
//         int getArea()
//             {
//                 return (width*height);
//             }
// };
// int main()
// {
//     rectangle Rect;
//     Rect.set_values(5,3);
//     cout << "Total area : " << Rect.getArea();
// }

// multiple inherit

// class student
// {
// protected:
//     int roll, m1, m2;

// public:
//     void get()
//     {
//         cout << "Enter the roll no : ";
//         cin >> roll;
//         cout << "Enter the marks : " << endl;
//         cin >> m1 >> m2;
//         cout << "m1 : " << m1 << endl
//              << "m2 : " << m2 << endl;
//     }
// };
// class curriculum
// {
// public:
//     int xm;

// public:
//     void get_xm()
//     {
//         cout << "Enter the extra curriculum mark ";
//         cin >> xm;
//         cout << xm << endl;
//     }
// };
// class avrage : public student, public curriculum
// {
//     float total;

// public:
//     void display()
//     {
//         total = (m1 + m2 + xm);
//         cout << "\nRoll no : " << roll << "\nTotal : " << total;
//     }
// };
// int main()
// {
//     avrage a1;
//     a1.get();
//     a1.get_xm();
//     a1.display();
//     return 0;
// }

// hierarical

// class a
// {
// public:
//     int x, y;
//     void getdata()
//     {
//         cout << "Enter values of x and y : \n";
//         cin >> x >> y;
//     }
// };
// class b : public a
// {
//     public:
//     void product()
//     {
//         cout << "Product = " << x * y << endl;
//     }
// };
// class c : public a
// {
//     public:
//         void sum()
//         {
//             cout << "Sum = " << x+y ;
//         }
// };
// int main()
// {
//     b obj1;
//     c obj2;
//     obj1.getdata();
//     obj1.product();
//     obj2.getdata();
//     obj2.sum();
//     return 0;
// }

// multilevel

// class base
// {
// public:
//     void display1()
//     {
//         cout << "Base class content.\n";
//     }
// };
// class derived1 : public base
// {
// public:
//     void display2()
//     {
//         cout << "Derived 1 class content.\n";
//     }
// };
// class derived2 : public derived1
// {
// public:
//     void display3()
//     {
//         cout << "Derived 2 class content.\n";
//     }
// };
// int main()
// {
//     derived2 D;
//     D.display1();
//     D.display2();
//     D.display3();
//     return 0;
// }

// hybrid

class stu
{
    int id;
    char name[20];
    public:
    void getstu()
    {
        cout << "Enter stuid and name : \n";
        cin >> id >> name;
        cout << id << endl << name;
    }
};
class marks : public stu
{
    protected:
    int m,p,c;
    public:
    void getmark ()
    {
        cout << "Enter 3 subject marks : \n";
        cin >> m >> p >> c;
        cout << m << endl << p << endl << c;
    }
};
class sports
{
    protected:
    int spmarks;
    public:
    void getsports()
    {
        cout << "Enter sports marks : \n";
        cin >> spmarks;
        cout << spmarks;
    }
};
class result : public marks,public sports
{
    int tot;
    float avg;
    public:
    void show()
    {
        tot = m+p+c;
        avg = tot/3.0;
        cout << "Total =" << tot << endl << "Average=" << avg << endl;
        cout << "Average + Sports marks = " << tot-avg;
    }
};
int main()
{

}