#include <iostream>
using namespace std;

class Node
{
public:
    Node *prev;
    int data;
    Node *next;
};

class DoublyLinkedList
{
    Node *head;

public:
    DoublyLinkedList()
    {
        head = NULL;
    }
    // blank
    void addfirst(int val)
    {
        Node *newNode = new Node();
        newNode->data = val;

        if (head == NULL)
        {
            newNode->prev = NULL;
            newNode->next = NULL;
            head = newNode;
        }
        else
        {
            newNode->prev = NULL;
            newNode->next = head;
            head->prev = newNode;
            head = newNode;
        }
    }

    void print()
    {
        Node *temp = head;
        Node *last = NULL;

        cout << "Forward Traversal" << endl;
        while (temp != NULL)
        {
            cout << temp->data << " ";
            if (temp->next == NULL)
            {
                last = temp;
            }
            temp = temp->next;
        }

        cout << endl << "backward Traversal" << endl;
        temp = last;
        while (temp != NULL)
        {
            cout << temp->data << " ";
            temp = temp->prev;
        }
    }
};

int main()
{
    DoublyLinkedList list;

    // cout << "Inserting Element : " << 10 << endl;
    // list.addfirst(10);
    // cout << "Inserting Element : " << 20 << endl;
    // list.addfirst(20);
    // cout << "Inserting Element : " << 30 << endl;
    // list.addfirst(30);
    // cout << "Inserting Element : " << 40 << endl;
    // list.addfirst(40);

    int n, x;
    cout << "Enter the range : ";
    cin >> n;
    for (int i = 0; i < n; i++)
    {
        cout << "Insert the element : ";
        cin >> x;
        list.addfirst(x);
    }
    list.print();
    return 0;
}
