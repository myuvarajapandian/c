#include <iostream>
using namespace std;

struct Node
{
    int num;
    Node *next;
};

struct Node *head = NULL;

void insert_Node(int y)
{
    struct Node *new_node = new_node;
    new_node->num = y;
    new_node->next = head;
    head = new_node;
}

void display_all_nodes()
{
    cout << "The list contains the data entered : \n";
    struct Node *temp = head;
    while (temp != NULL)
    {
        cout << temp->num << " ";
        temp = temp->next;
    }
    cout << endl;
}

int main()
{
    // insert_Node(1);
    // insert_Node(2);
    // insert_Node(3);
    // insert_Node(4);
    // insert_Node(5);
    int i, r, y;
    cout << "enter the range : ";
    cin >> r;
    for (i = 0; i < r; i++)
    {
        if (i < r)
        {
            cout << "Enter the elements : ";
            cin >> y;
            insert_Node(y);
        }
        else
            break;
    }
    display_all_nodes();
    return 0;
}
