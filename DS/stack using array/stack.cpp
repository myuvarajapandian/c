#include<iostream>
using namespace std;
int top = -1;

void push (int stack [],int x,int n)
{
    if (top == n-1)
    {
        cout << "Stack Overflow condition!" << endl;
    }
    else
    {
        top = top + 1;
        stack[top] = x;
    }
}

bool isEmpty()
{
    if (top == -1)
        return true;
    else
        return false;
}

void pop ()
{
    if (isEmpty())
    {
        cout << "Stack Underflow condition!" << endl;
    }
    else
    {
        top = top - 1;
    }
}

int size()
{
    return top + 1; 
}
int topElement(int stack[])
{
    return stack[top];
}

int main()
{
    int stack [3];
    push(stack,5,3);
    cout << "Current size of stack is " << size() << endl;
    push(stack,10,3);
    push(stack,15,3);
    cout << "Current size of stack is " << size() << endl;
    push(stack,20,3);
    cout << "Current top element in stack is " << topElement(stack) << endl;

    for (int i = 0; i < 3; i++)
    {
        pop();
        cout << "Current size of stack is " << size() << endl;
    }
    pop();
}