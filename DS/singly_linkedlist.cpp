#include <iostream>
using namespace std;

// class Node
// {
// public:
//     Node *prev;
//     int data;
//     Node *next;
// };

// int main()
// {
//     Node *head, *middle, *last;

//     head = new Node();
//     middle = new Node();
//     last = new Node();

//     head->data = 10;
//     middle->data = 20;
//     last->data = 30;

//     head->prev = NULL;
//     head->next = middle;
//     middle->prev = head;
//     middle->next = last;
//     last->prev = middle;
//     last->next = NULL;

//     Node *temp = head;
//     cout << "Forword traversal" << endl;
//     while (temp != NULL)
//     {
//         cout << temp -> data << endl;
//         temp = temp -> next;
//     }

//     temp = last;
//     cout << "Background Traversal" << endl;
//     while (temp != NULL)
//     {
//         cout << temp -> data << endl;
//         temp = temp -> prev;
//     }
//     return 0;
// }

struct Node {
    int data;
    Node *prev;
    Node *next;
};

int main()
{
    Node *head =NULL;
    Node *tail =NULL;
    
    int n;
    cout << "Enter the no. of nodes : ";
    cin >> n;

    for (int i = 0; i < n; i++)
    {
        int val;
        cout << "Enter the element " << i+1 << ":"  << endl;
        cin >> val;

        Node *newNode = new Node;
        newNode -> data = val;
        newNode -> prev = tail;
        newNode -> next = NULL;

        if (tail != NULL)
        {
            tail -> next = newNode;
        }
        
        tail = newNode;
        if (head == NULL)
        {
            head = newNode;
        }
    }

    cout << "List in forward traversal : ";
    Node *currNode = head;
    while (currNode != NULL)
    {
        cout << currNode -> data << " ";
        currNode = currNode -> next; 
    }

    cout << "\nList in backward traversal : ";
    currNode = tail;
    while (currNode != NULL)
    {
        cout << currNode -> data << " ";
        currNode = currNode -> prev; 
    }
        
}