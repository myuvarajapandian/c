#include<iostream>
using namespace std;

// copy constructor 

class sample
{
private:
    int x,y;
public:
    sample(int x1,int y1)
    {
        x = x1; y = y1;
    }
    sample (const sample &s2)
    {
        x = s2.x ; y = s2.y;
    }
    void display()
    {
        cout << x << " " << y << endl;
    }
};
int main()
{
    sample s1 = sample(10,15);  //  
    sample s2 = s1;             // copying object
    s1.display();
    s2.display();
    return 0;
}
