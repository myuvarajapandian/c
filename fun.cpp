#include<iostream>
using namespace std;

// psudo 1 recursion

// int fun(int a)
// {
//     if (a < 1)
//         return 1;
//     else
//         {
//             cout << a;
//             fun(a-2);
//             cout << a;
//             return 1;
//         }
    
// }
// int main()
// {
//     int a;
//     a = 3;
//     fun(a);
// }

// psudo 2 recursion

// int f1(int n,int r)
// {
//     if (n > 0)
//         {
//             cout << n << "," << r << endl;
//             return (n - r + f1(n/3,13));
//         }
//     else
//         return 0;
// }
// int main()
// {
//     int r,n;
//     n = 39;
//     r = 13;
//     f1(n,r);
// }